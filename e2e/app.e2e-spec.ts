import { SignalMessengerPage } from './app.po';

describe('signal-messenger App', () => {
  let page: SignalMessengerPage;

  beforeEach(() => {
    page = new SignalMessengerPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
