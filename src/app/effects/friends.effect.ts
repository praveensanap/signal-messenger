/**
 * Created by praveensanap on 23/7/17.
 */

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/takeUntil';

import 'rxjs/add/operator/toArray';

import { Injectable } from '@angular/core';
import { Effect, Actions, toPayload } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { empty } from 'rxjs/observable/empty';
import { of } from 'rxjs/observable/of';
import 'rxjs/add/operator/startWith';

import { FakeSignalApi } from '../services/SignalApi';
import * as friend from '../actions/friends.action';
import {Friend} from '../models/friend.model';

@Injectable()
export class FriendsEffects {

  @Effect()
  search$: Observable<Action> = this.actions$
    .ofType(friend.SEARCH)
    .debounceTime(1000) // Fake debounce to mock http call
    .map(toPayload)
    .switchMap(query => {
      if (query === '') {
        return empty();
      }

      const nextSearch$ = this.actions$.ofType(friend.SEARCH).skip(1);

      return this.api.searchFriends(query)
        .takeUntil(nextSearch$)
        .map(friends => new friend.SearchCompleteAction(friends))
        .catch(() => of(new friend.SearchCompleteAction([])));
    });

  // This effect is called when the app is initiazed
  @Effect()
  loadFriends$: Observable<Action> = this.actions$
    .ofType(friend.LOAD)
    .debounceTime(1000) // Fake debounce to mock http call
    .switchMap((query) =>
      this.api.retrieveFriends(query.payload)
        .map((friends: Friend[]) => new friend.LoadSuccessAction(friends))
        .catch(error => of(new friend.LoadFailAction(error)))
    );

  constructor(private actions$: Actions, private api: FakeSignalApi) { }
}
