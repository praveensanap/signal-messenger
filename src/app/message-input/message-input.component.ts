import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {Store} from '@ngrx/store';
import {Message , MessageState} from '../models/message.model';
import * as conversationActions from '../actions/conversation.action';
import * as fromRoot from '../reducers/index.reducer';
import {Friend} from "../models/friend.model";

@Component({
  selector: 'app-message-input',
  templateUrl: './message-input.component.html',
  styleUrls: ['./message-input.component.css']
})
export class MessageInputComponent implements OnInit {

  nextMessage = '';
  sendControl: FormControl = new FormControl();
  selectedFriend: Friend;

  constructor(private store: Store<fromRoot.State>) {
    store.select(fromRoot.getCurrThreadDraftMsg).subscribe(e => {
      console.log(e);
      // this.nextMessage = e;
    });
    store.select(fromRoot.getSelectedFriend).subscribe(
      e => this.selectedFriend = e
    );
    this.sendControl.valueChanges
      .debounceTime(300)
      .subscribe(msg => {
        this.store.dispatch(new conversationActions.TypeMessageAction(msg));
        this.nextMessage = msg
      });
  }

  sendMsg() {
    const msgObj: Message = {
      id: '123', // TODO uuid
      fromFriend: this.selectedFriend,
      status: MessageState.SENT,
      content: this.nextMessage,
      timestamp: ''
    };
    this.store.dispatch(new conversationActions.SendMessageAction({
      friendId: this.selectedFriend.id,
      message: msgObj
    }));
    this.nextMessage = '';
  }

  ngOnInit() {
  }

}
