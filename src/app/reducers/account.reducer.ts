import {Account} from '../models/account.model'
import * as account from '../actions/account.action';

export interface State {
  settings: Account;
  loggedIn: boolean;
}

const initialState = {
  settings: null,
  loggedIn: true
}

export function reducer(state: State = initialState , action: account.Action) {
  switch (action.type) {
    case account.LOGIN_SUCCESS: {
      return Object.assign({}, state, {
        loggedIn: true
      });
    }
    case account.LOGIN_FAIL: {
      return Object.assign({}, state, {
        loggedIn: false
      });
    }
    default: {
      return state;
    }

  }
}

export const getLoginStatus = (state: State) => state.loggedIn;

