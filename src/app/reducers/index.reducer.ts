
import { createSelector } from 'reselect';
import { ActionReducer } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { environment } from '../../environments/environment';

import { compose } from '@ngrx/core/compose';

import { storeFreeze } from 'ngrx-store-freeze';

import { combineReducers } from '@ngrx/store';

import { Friend } from '../models/friend.model';


import * as fromAccount from './account.reducer';
import * as fromFriends from './friends.reducer';
import * as fromSettings from './settings.reducer';
import * as fromConversation from './conversation.reducer';

export interface State {
  account: fromAccount.State;
  friends: fromFriends.State;
  conversations: fromConversation.State;
  settings: fromSettings.State;
  router: fromRouter.RouterState;
}

const reducers = {
  account: fromAccount.reducer,
  friends: fromFriends.reducer,
  conversations: fromConversation.reducer,
  settings: fromSettings.reducer,
  router: fromRouter.routerReducer,
};

const developmentReducer: ActionReducer<State> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<State> = combineReducers(reducers);

export function reducer(state: any, action: any) {
  if (environment.production) {
    return productionReducer(state, action);
  } else {
    return developmentReducer(state, action);
  }
}

export const getFriendsState = (state: State) => state.friends;

export const getFriendEntities = createSelector(getFriendsState, fromFriends.getFriends);

export const getSearchFriendIds = createSelector(getFriendsState, fromFriends.getIds);
export const getSearchQuery = createSelector(getFriendsState, fromFriends.getQuery);
export const getLoading = createSelector(getFriendsState, fromFriends.getLoading);

export const getSearchResults = createSelector(getFriendEntities, getSearchFriendIds, (books, searchIds) => {
  return searchIds.map(id => books[id]);
});

export const getSelectedFriend = createSelector(getFriendsState, fromFriends.getSelectedFriend);

//******** conversation selectors ********//
export const getConversationsState = (state: State) => {
  return state.conversations;
};

export const getCurrThreadDraftMsg = createSelector(getSelectedFriend, getConversationsState,
  (selectedFriend: Friend, conversationState: fromConversation.State) => {
    if (!selectedFriend) return '';
    return fromConversation.getDraftMsgForFriend(conversationState, selectedFriend.id);
});

export const getCurrThreadMsgs = createSelector(getSelectedFriend, getConversationsState,
  (selectedFriend: Friend, conversationState: fromConversation.State) => {
    return fromConversation.getMsgsForFriend(conversationState, selectedFriend.id);
});


export const getAccountState = (state: State) => state.account;

export const getLoginStatus = createSelector(getAccountState, fromAccount.getLoginStatus);
