
// TODO rename Conversation Stack
import {Friend} from '../models/friend.model';
import * as friend from '../actions/friends.action';

export interface State {
  friends: Friend[];
  selected: Friend;
  ids: Array<string>;
  loading: boolean,
  query: string
}

const initialState: State = {
  friends: [],
  selected: null,
  ids: [],
  loading: false,
  query: ''
};

export function reducer(state = initialState , action: friend.Actions) {
  switch (action.type) {
    case friend.SEARCH: {
      const query = action.payload;
      console.log('searching for ' + query);
      if (query === '') {
        return {
          loading: false,
        };
      }

      return Object.assign({}, state, {
        query,
        loading: true
      });
    }

    case friend.LOAD: {
      return Object.assign({}, state, {
        loading: true
      });
    }

    case friend.SEARCH_COMPLETE: {
      const friends = action.payload;
      console.log('search complete with ' + friends)
      return {
        ids: friends.map(friend => friend.id),
        loading: false,
        query: state.query
      };
    }

    case friend.LOAD_SUCCESS: {
      const friends = action.payload;
      console.log(friends);
      return Object.assign({}, state, {
        friends: friends,
        loading: false
      });
    }

    case friend.SELECT: {
      const friend = action.payload;
      return Object.assign({}, state, {
        selected: friend,
      });
    }

    default: {
      return state;
    }
  }
}

export const getFriends = (state: State) => state.friends;

export const getSelectedFriend = (state: State) => state.selected;

export const getIds = (state: State) => state.ids;

export const getQuery = (state: State) => state.query;

export const getLoading = (state: State) => state.loading;
