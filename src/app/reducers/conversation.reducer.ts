
import {Conversation} from '../models/conversation.model';
import {Message} from '../models/message.model';

import * as fromConversation from '../models/conversation.model';
import * as conversation from '../actions/conversation.action';

export interface State {
    conversations: Map<string, Conversation>;
    loading: boolean;
    query?: string;
}

const initialState: State = {
    conversations: new Map(),
    loading: false,
    query: ''
};

function createNewState(friendId: string, state: State, partialConversation: any) {
    let conversations = state.conversations;
    // TODO make clone of conversations
    if (!conversations.has(friendId)) {
        conversations.set(friendId, fromConversation.getDefaultConversation(friendId));
    }

    let friendsConversation = conversations.get(friendId);
    friendsConversation = Object.assign({}, friendsConversation, partialConversation);
    // TODO clone map before setting
    conversations.set(friendId, friendsConversation);

    return Object.assign({}, state, {
        conversations: conversations
    });
}

export function reducer(state: State = initialState, action: conversation.Actions) {
    switch (action.type) {
        case conversation.SELECT_CONVERSATION: {
            // const conversationId = action.payload;

            // let conversations = state.conversations;
            // if (conversations.has(conversationId)) {

            // }

            // let conversationIndex = conversations
            //             .map((conversation: Conversation) => conversation.id)
            //             .indexOf(conversationId);
                        
            // if (conversationIndex === -1) {
            //     conversationIndex = conversations.length;
            //     conversations = [...conversations, fromConversation.getDefaultConversation(conversationId)];
            // }            

            // return Object.assign({}, state, {
            //     conversations: conversations,
            //     selectedConversation: conversations[conversationIndex]
            // });
            return state;
        }

        case conversation.TYPE_MESSAGE: {
            const message = action.payload.message;
            const friendId = action.payload.friendId;

            let conversations = state.conversations;
            // TODO make clone of conversations
            if (!conversations.has(friendId)) {
                conversations.set(friendId, fromConversation.getDefaultConversation(friendId));
            }

            let friendsConversation = conversations.get(friendId);
            friendsConversation = Object.assign({}, friendsConversation, {draftMsg: message});
            // TODO clone map before setting
            conversations.set(friendId, friendsConversation);

            return Object.assign({}, state, {
                conversations: conversations
            });
        }

        case conversation.SEND_MESSAGE: {
            const message = action.payload.message;
            const friendId = action.payload.friendId;

            let conversations = state.conversations;
            // TODO make clone of conversations
            if (!conversations.has(friendId)) {
                conversations.set(friendId, fromConversation.getDefaultConversation(friendId));
            }

            let friendsConversation = conversations.get(friendId);
            friendsConversation = Object.assign({}, friendsConversation, {
                messages: [...friendsConversation.messages, message]
            });
            // TODO clone map before setting
            conversations.set(friendId, friendsConversation);

            return Object.assign({}, state, {
                conversations: conversations
            });
        }

        case conversation.RECIEVE_MESSAGE: {
            const message = action.payload;

            // return Object.assign({}, state, {
            //     selectedConversation: Object.assign({}, state.selectedConversation, {
            //         lastMsg: message,
            //         messages: [...state.selectedConversation.messages, message]
            //     })
            // });
        }

        case conversation.LOAD_MORE: {
            // const messages = action.payload;

            // return Object.assign({}, state, {
            //     selectedConversation: Object.assign({}, state.selectedConversation, {
            //         messages: [...messages, ...state.selectedConversation.messages]
            //     })
            // });
        }

        case conversation.DELETE_MESSAGE: {
            const delMsg = action.payload;

            // return Object.assign({}, state, {
            //     selectedConversation: Object.assign({}, state.selectedConversation, {
            //         messages: state.selectedConversation.messages
            //                     .filter((message: Message) => message.id !== delMsg.id)
            //     })
            // });
        }

        default: {
            return state;
        }
    }
}

export const getMsgsForFriend = (state: State, friendId: string) => {
    let conversations = state.conversations;
    if (!conversations.has(friendId)) {
        return [];
    }
    return conversations.get(friendId).messages;
};

export const getDraftMsgForFriend = (state: State, friendId: string) => {
    let conversations = state.conversations;
    if (!conversations.has(friendId)) {
        return '';
    }
    return conversations.get(friendId).draftMsg;
};