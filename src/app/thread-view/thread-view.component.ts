import { Component, OnInit } from '@angular/core';

import {Message} from '../models/message.model';

import * as fromRoot from '../reducers/index.reducer';
import {Observable} from 'rxjs/Rx';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-thread-view',
  templateUrl: './thread-view.component.html',
  styleUrls: ['./thread-view.component.css']
})
export class ThreadViewComponent implements OnInit {
  messages$: Observable<Message[]>;

  constructor(private store: Store<fromRoot.State>) {
    this.messages$ = store.select(fromRoot.getCurrThreadMsgs);

  }

  ngOnInit() {
  }

}
