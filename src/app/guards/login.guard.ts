/**
 * Created by praveensanap on 2/8/17.
 */


import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {SignalApiService} from '../libtextsecure/signal-api.service';
import {Observable} from 'rxjs/Rx';

import * as fromRoot from '../reducers/index.reducer'
/**
 * Guards are hooks into the route resolution process, providing an opportunity
 * to inform the router's navigation process whether the route should continue
 * to activate this route. Guards must return an observable of true or false.
 */
@Injectable()
export class LoginGuard implements CanActivate {
  constructor(
    private store: Store<fromRoot.State>,
    private signalApi: SignalApiService,
    private router: Router
  ) { }


  isLoggedIn(): Observable<boolean> {
    return this.store.select(fromRoot.getLoginStatus)
      .take(1);
  }

  canActivate(): Observable<boolean> {
    return this.isLoggedIn().switchMap(
      (isLoggedin) => {
        if (isLoggedin) {
          return Observable.of(true);
        }else {
          this.router.navigate(['/login']);
          return Observable.of(false);
        }
      }
    )
  }
}
