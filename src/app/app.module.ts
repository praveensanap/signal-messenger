import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import 'hammerjs';
import {
  MdButtonModule,
  MdCardModule, MdIconModule, MdInputModule, MdMenuModule, MdProgressBarModule, MdSidenavModule,
  MdToolbarModule
} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import { FriendMiniComponent } from './friend-mini/friend-mini.component';
import { MessageViewComponent } from './message-view/message-view.component';
import { ConversationStackComponent } from './conversation-stack/conversation-stack.component';
import {reducer} from './reducers/index.reducer';
import {StoreModule} from '@ngrx/store';
import {RouterStoreModule} from '@ngrx/router-store';
import {routes} from './routes';
import {RouterModule} from '@angular/router';
import { SearchConverstionComponent } from './search-converstion/search-converstion.component';
import { EffectsModule } from '@ngrx/effects';
import {FriendsEffects} from './effects/friends.effect';
import {FakeSignalApi} from './services/SignalApi';
import {HttpModule} from '@angular/http';
import {AccountManager} from './libtextsecure/account-manager.service';
import { SignalAppComponent } from './signal-app/signal-app.component';
import { SignalRegistrationComponent } from './signal-registration/signal-registration.component';
import { SignalLoginComponent } from './signal-login/signal-login.component';
import {LoginGuard} from './guards/login.guard';
import {SignalApiService} from './libtextsecure/signal-api.service';
import { WelcomeMessageComponent } from './welcome-message/welcome-message.component';
import { ThreadViewComponent } from './thread-view/thread-view.component';
import { MessageInputComponent } from './message-input/message-input.component';

@NgModule({
  declarations: [
    AppComponent,
    FriendMiniComponent,
    MessageViewComponent,
    ConversationStackComponent,
    SearchConverstionComponent,
    SignalAppComponent,
    SignalRegistrationComponent,
    SignalLoginComponent,
    WelcomeMessageComponent,
    ThreadViewComponent,
    MessageInputComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MdInputModule,
    MdMenuModule,
    MdButtonModule,
    MdCardModule,
    MdSidenavModule,
    MdToolbarModule,
    MdIconModule,
    MdProgressBarModule,
    StoreModule.provideStore(reducer),
    RouterStoreModule.connectRouter(),
    RouterModule.forRoot(routes),
    EffectsModule.run(FriendsEffects)
  ],
  providers: [FakeSignalApi , AccountManager , LoginGuard , SignalApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
