import { TestBed, inject } from '@angular/core/testing';

import { SignalApiService } from './signal-api.service';

describe('SignalApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SignalApiService]
    });
  });

  it('should be created', inject([SignalApiService], (service: SignalApiService) => {
    expect(service).toBeTruthy();
  }));
});
