import { Injectable } from '@angular/core';
import {Http} from '@angular/http';

@Injectable()
export class AccountManager {

  constructor(private http: Http) {
  }
  requestVoiceVerification(){};
  requestSMSVerification(){};
  registerSingleDevice(){};
  registerSecondDevice(){};
  refreshPreKeys(){};
  rotateSignedPreKey(){};
  queueTask(){};
  cleanSignedPreKeys(){};
  createAccount(){};
  generateKeys(){};
  registrationDone(){};

}
