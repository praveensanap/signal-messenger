import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class SignalApiService {

  SERVER_URL = 'https://textsecure-service-staging.whispersystems.org';
  SERVER_PORTS: number[] = [80, 4433, 8443];
  lastPort = 0;
  URL_CALLS = {
    accounts   : 'v1/accounts',
    devices    : 'v1/devices',
    keys       : 'v2/keys',
    signed     : 'v2/keys/signed',
    messages   : 'v1/messages',
    attachment : 'v1/attachments'
  };

  getPort() {
    const port = this.lastPort;
    this.lastPort += 1;
    return this.SERVER_PORTS[port];
  }

  getUrl(call: string ) {
    return this.SERVER_URL +  ':' + this.getPort() + '/' + this.URL_CALLS[call];
  }
  requestVerificationSMS(number: number): Promise<boolean> {
    const url = this.getUrl(this.URL_CALLS.accounts)  + '/sms/code/' + number;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as boolean)
      .catch(this.handleError);
  }
  requestVerificationVoice(){}
  confirmCode(){}
  getDevices(){}
  registerKeys(){}
  setSignedPreKey(){}
  getMyKeys(){}
  getKeysForNumber(){}
  sendMessages(){}
  getAttachment(){}
  putAttachment(){}
  getMessageSocket(){}
  getProvisioningSocket(){}

  handleError(e: any) {
    const code = e.code;
    if (code === 200) {
      // happens sometimes when we get no response
      // (TODO: Fix server to return 204? instead)
      return null;
    }
    let message = '';
    switch (code) {
      case -1:
        message = 'Failed to connect to the server, please check your network connection.';
        break;
      case 413:
        message = 'Rate limit exceeded, please try again later.';
        break;
      case 403:
        message = 'Invalid code, please try again.';
        break;
      case 417:
        // TODO: This shouldn't be a thing?, but its in the API doc?
        message = 'Number already registered.';
        break;
      case 401:
        message = 'Invalid authentication, most likely someone re-registered and invalidated our registration.';
        break;
      case 404:
        message = 'Number is not registered.';
        break;
      default:
        message = 'The server rejected our query, please file a bug report.';
    }
    e.message = message
    throw e;
  }

  constructor(private http: Http) { }

}
