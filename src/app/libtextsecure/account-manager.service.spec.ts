import { TestBed, inject } from '@angular/core/testing';

import { AccountManager } from './account-manager.service';

describe('AccountManager', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountManager]
    });
  });

  it('should be created', inject([AccountManager], (service: AccountManager) => {
    expect(service).toBeTruthy();
  }));
});
