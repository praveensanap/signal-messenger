import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignalRegistrationComponent } from './signal-registration.component';

describe('SignalRegistrationComponent', () => {
  let component: SignalRegistrationComponent;
  let fixture: ComponentFixture<SignalRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignalRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignalRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
