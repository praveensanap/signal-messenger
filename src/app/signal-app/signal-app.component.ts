import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';

import {Friend} from '../models/friend.model';

import * as fromRoot from '../reducers/index.reducer';


@Component({
  selector: 'app-signal-app',
  templateUrl: './signal-app.component.html',
  styleUrls: ['./signal-app.component.css']
})
export class SignalAppComponent implements OnInit {
  selectedFriend: Friend;
  constructor(private store: Store<fromRoot.State>) {
    store.select(fromRoot.getSelectedFriend).subscribe(
      e => this.selectedFriend = e
    );
  }
  ngOnInit() {
  }
}
