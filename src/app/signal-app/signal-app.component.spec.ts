import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignalAppComponent } from './signal-app.component';

describe('SignalAppComponent', () => {
  let component: SignalAppComponent;
  let fixture: ComponentFixture<SignalAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignalAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignalAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
