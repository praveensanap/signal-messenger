import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {Friend} from '../models/friend.model';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/mergeMap';

import { environment } from '../../environments/environment';

/**
 * Created by praveensanap on 23/7/17.
 */


@Injectable()
export class FakeSignalApi {
  private API_PATH = environment.API;

  constructor(private http: Http) {}

  searchFriends(queryTitle: string): Observable<Friend[]> {
    // return this.http.get(`${this.API_PATH}?q=${queryTitle}`)
    //   .map(res => res.json().items || []);

    const friends = [];
    const aai = {
      id: '2312311',
      dp: '/aai',
      name: 'Aai',
      lastMessage: 'Kidhar hai',
      lastSeen: '3 hours',
      conversationId: 121
    };
    const vishnu = {
      id: '2312312',
      dp: '/vishnu',
      name: 'Vishnu',
      lastMessage: 'Der',
      lastSeen: '3 hours',
      conversationId: 122
    };
    friends.push(vishnu);
    friends.push(aai);
    console.log('loading');
    return Observable.of(friends).debounceTime(10000);
  }

  retrieveFriends(volumeId: string): Observable<Friend[]> {
    // return this.http.get(`${this.API_PATH}/${volumeId}`)
    //   .map(res => res.json());
    const friends = [];
    const aai = [{
      id: '2312311',
      dp: '/aai',
      name: 'Aai',
      lastMessage: 'Kidhar hai',
      lastSeen: '3 hours',
      conversationId: 121
    }, {
      id: '2312312',
      dp: '/vishnu',
      name: 'Vishnu',
      lastMessage: 'Der',
      lastSeen: '3 hours',
      conversationId: 122
    }, {
      id: '2312313',
      dp: '/vishnu',
      name: 'Jayesh',
      lastMessage: 'Der',
      lastSeen: '3 hours',
      conversationId: 123
    }, {
      id: '2312314',
      dp: '/vishnu',
      name: 'Vishnu',
      lastMessage: 'Der',
      lastSeen: '3 hours',
      conversationId: 124
    }, {
      id: '2312315',
      dp: '/vishnu',
      name: 'Vishnu',
      lastMessage: 'Der',
      lastSeen: '3 hours',
      conversationId: 125
    }, {
      id: '2312316',
      dp: '/vishnu',
      name: 'Vishnu',
      lastMessage: 'Der',
      lastSeen: '3 hours',
      conversationId: 126
    }]
    aai.forEach(e => friends.push(e));
    console.log('loading');
    return Observable.of(friends)
      .debounceTime(5000)
  }
}
