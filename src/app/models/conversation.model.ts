
import { Message } from './message.model';
import { Friend } from './friend.model';

import * as fromMessage from './message.model';

export class Conversation {
    id: string;
    
    // index 0 is latest message
    // Queue data structure for easy sending and receiving and messages
    // Load more message is rare
    messages: Message[];
    
    // number of messages loaded
    limit: number;

    lastMsg?: Message;
    read?: boolean;
    lastAccessed?: string;
    draftMsg?: string;
}

export const defaultConversation = {
    id: 1, // TODO uuid
    messages: [],
    limit: 0,
    lastMsg: '',
    read: true,
    draftMsg: ''
}

export function getDefaultConversation(conversationId: string) {
    return {
        id: conversationId,
        messages: [],
        limit: 0,
        lastMsg: fromMessage.defaultMessage,
        read: true,
        lastAccessed: '', // TODO fix this
        draftMsg: ''
    };
}