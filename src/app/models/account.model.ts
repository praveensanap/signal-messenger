/**
 * Created by praveensanap on 2/8/17.
 */

export interface Account {
  id: string;
  username: string,
  password: string
}
