import { Friend } from './friend.model';

export interface Message {
  id: string,
  fromFriend: Friend,
  status: MessageState,
  content: string,
  timestamp: string
}

export enum MessageState  {
  SENDING,
  SENT,
  READ
}

export const defaultMessage = {
  id: '',
  fromFriend: null,
  status: MessageState.READ,
  content: '',
  timestamp: ''
}