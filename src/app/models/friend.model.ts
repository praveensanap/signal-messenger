export interface Friend {
  id: string,
  dp: string,
  name: string,
  lastSeen: string
}

