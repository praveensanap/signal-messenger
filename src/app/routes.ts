import { Routes } from '@angular/router';
import {SignalAppComponent } from './signal-app/signal-app.component';
import {LoginGuard} from './guards/login.guard';
import {SignalLoginComponent} from './signal-login/signal-login.component';



export const routes: Routes = [
  {path: '', redirectTo: 'signal', pathMatch: 'full'},
  {
    path: 'signal',
    canActivate: [ LoginGuard ],
    component: SignalAppComponent
  },
  {
    path: 'login',
    component : SignalLoginComponent
  }
];
