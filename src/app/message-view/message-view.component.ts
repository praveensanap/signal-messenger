import {Component, Input, OnInit} from '@angular/core';
import {Message} from '../models/message.model';

@Component({
  selector: 'app-message-view',
  templateUrl: './message-view.component.html',
  styleUrls: ['./message-view.component.css']
})
export class MessageViewComponent implements OnInit {

  @Input()
  message: Message;

  constructor() { }

  ngOnInit() {
  }

}
