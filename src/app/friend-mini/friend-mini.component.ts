import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Friend} from '../models/friend.model';

@Component({
  selector: 'app-friend-mini',
  templateUrl: './friend-mini.component.html',
  styleUrls: ['./friend-mini.component.css']
})
export class FriendMiniComponent implements OnInit {

  @Input()
  friend: Friend;
  @Output()
  selected: EventEmitter<boolean>;

  constructor() { }

  ngOnInit() {
  }

}
