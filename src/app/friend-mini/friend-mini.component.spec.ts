import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FriendMiniComponent } from './friend-mini.component';

describe('FriendMiniComponent', () => {
  let component: FriendMiniComponent;
  let fixture: ComponentFixture<FriendMiniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FriendMiniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FriendMiniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
