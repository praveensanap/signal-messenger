import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationStackComponent } from './conversation-stack.component';

describe('ConversationStackComponent', () => {
  let component: ConversationStackComponent;
  let fixture: ComponentFixture<ConversationStackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationStackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
