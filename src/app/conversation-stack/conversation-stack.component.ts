import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs/Rx';
import {Friend} from '../models/friend.model';
import * as fromRoot from '../reducers/index.reducer';
import {Store} from '@ngrx/store';


import * as actions from '../actions/friends.action';
import {Subject} from 'rxjs/Rx';
import * as conversationActions from '../actions/conversation.action';

@Component({
  selector: 'app-conversation-stack',
  templateUrl: './conversation-stack.component.html',
  styleUrls: ['./conversation-stack.component.css']
})
export class ConversationStackComponent implements OnInit {
  friends$: Observable<Friend[]>;
  searchResult$: Observable<Friend[]>;
  searchQuery$: Observable<string>;
  loading$: Observable<boolean>;

  constructor(private store: Store<fromRoot.State>) {
    this.searchQuery$ = store.select(fromRoot.getSearchQuery).take(1);
    this.friends$ = store.select(fromRoot.getFriendEntities);
    this.searchResult$ = store.select(fromRoot.getSearchResults);
    this.loading$ = store.select(fromRoot.getLoading);
  }

  ngOnInit() {
    this.store.dispatch(new actions.LoadAction())
  }

  search(query: string) {
    this.store.dispatch(new actions.SearchAction(query));
  }

  select(friend: Friend) {
    this.store.dispatch(new actions.SelectAction(friend))
  }

}
