import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signal-login',
  templateUrl: './signal-login.component.html',
  styleUrls: ['./signal-login.component.css']
})
export class SignalLoginComponent implements OnInit {

  call() {}
  voice() {}
  verify() {}

  constructor() { }

  ngOnInit() {
  }

}
