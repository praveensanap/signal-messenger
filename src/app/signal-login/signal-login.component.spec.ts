import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignalLoginComponent } from './signal-login.component';

describe('SignalLoginComponent', () => {
  let component: SignalLoginComponent;
  let fixture: ComponentFixture<SignalLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignalLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignalLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
