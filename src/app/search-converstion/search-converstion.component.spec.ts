import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchConverstionComponent } from './search-converstion.component';

describe('SearchConverstionComponent', () => {
  let component: SearchConverstionComponent;
  let fixture: ComponentFixture<SearchConverstionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchConverstionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchConverstionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
