import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-search-converstion',
  templateUrl: './search-converstion.component.html',
  styleUrls: ['./search-converstion.component.css']
})
export class SearchConverstionComponent implements OnInit {
  @Input() query = '';
  @Input() searching = false;
  @Output() search = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

}
