/**
 * Created by praveensanap on 23/7/17.
 */

import { Action } from '@ngrx/store';
import { Friend } from '../models/friend.model';

export const SEARCH =           '[Friend] Search';
export const SEARCH_COMPLETE =  '[Friend] Search Complete';
export const LOAD =             '[Friend] Load';
export const LOAD_SUCCESS =     '[Friend] Load Success';
export const LOAD_FAILED =      '[Friend] Load Failed';
export const SELECT =           '[Friend] Select';



export class SearchAction implements Action {
  readonly type = SEARCH;

  constructor(public payload: string) { }
}

export class SearchCompleteAction implements Action {
  readonly type = SEARCH_COMPLETE;

  constructor(public payload: Friend[]) { }
}

export class LoadAction implements Action {
  readonly type = LOAD;
}

export class LoadSuccessAction implements Action {
  readonly type = LOAD_SUCCESS;

  constructor(public payload: Friend[]) { }
}

export class LoadFailAction implements Action {
  readonly type = LOAD_FAILED;

  constructor(public payload: any) { }
}

export class SelectAction implements Action {
  readonly type = SELECT;

  constructor(public payload: Friend) { }
}

export type Actions
  = SearchAction
  | SearchCompleteAction
  | LoadAction
  | LoadSuccessAction
  | LoadFailAction
  | SelectAction;
