
import { Action } from '@ngrx/store';
import { Conversation } from '../models/conversation.model';
import { Message } from '../models/message.model';

export const SELECT_CONVERSATION = '[Conversation] Select conversation';

export const TYPE_MESSAGE =     '[Conversation] Type message';
export const SEND_MESSAGE =     '[Conversation] Send message';
export const RECIEVE_MESSAGE =  '[Conversation] Receive message in conversation';

export const LOAD_MORE =        '[Conversation] Load more messages';
export const DELETE_MESSAGE =   '[Converstaion] delete message in conversation';

export const LOAD_MORE_LIMIT =  14;

export class SelectConversationAction implements Action {
    readonly type = SELECT_CONVERSATION;

    // conversationId to select
    constructor(public payload: string) { }
}

export class TypeMessageAction implements Action {
    readonly type = TYPE_MESSAGE;

    constructor(public payload: {message: string, friendId: string}) { }
}

export class SendMessageAction implements Action {
    readonly type = SEND_MESSAGE;

    constructor(public payload: {message: Message, friendId: string}) { }
}

export class RecieveMessageAction implements Action {
    readonly type = RECIEVE_MESSAGE;

    constructor(public payload: Message) { }
}

export class LoadMoreAction implements Action {
    readonly type = LOAD_MORE;

    constructor(public payload: Message[]) { }
}

export class DeleteMessageAction implements Action {
    readonly type = DELETE_MESSAGE;

    constructor(public payload: Message) { }
}

export type Actions =
    SelectConversationAction |
    TypeMessageAction |
    SendMessageAction |
    RecieveMessageAction |
    LoadMoreAction |
    DeleteMessageAction;