import {Action} from "@ngrx/store";
/**
 * Created by praveensanap on 2/8/17.
 */

export const LOGIN =                    '[Account] Login';
export const LOGIN_SUCCESS =            '[Account] Login Success';
export const LOGIN_FAIL =               '[Account] Login FAIL';

export const REGISTER =                 '[Account] REGISTER';
export const REGISTER_SUCCESS =         '[Account] REGISTER Success';
export const REGISTER_FAIL =            '[Account] REGISTER FAIL';

export const SEND_VERIFICATION_SMS =    '[Account] Send Verification SMS';
export const SEND_VERIFICATION_VOICE =  '[Account] Send Verification VOICE';
export const CONFIRM_CODE =             '[Account] Confirm Verification Code';


export class LoginAction implements Action {
  readonly type = LOGIN;
  constructor(public payload: any) { }
}

export class LoginSuccessAction implements Action {
  readonly type = LOGIN_SUCCESS;
  constructor(public payload: any) { }
}

export class LoginFailAction implements Action {
  readonly type = LOGIN_FAIL;
  constructor(public payload: any) { }
}


export class RegistrationAction implements Action {
  readonly type = REGISTER;
  constructor(public payload: any) { }
}

export class RegistrationSuccessAction implements Action {
  readonly type = REGISTER_SUCCESS;
  constructor(public payload: any) { }
}

export class RegistrationFailAction implements Action {
  readonly type = REGISTER_FAIL;
  constructor(public payload: any) { }
}

export class SendVerficationSmsAction implements Action {
  readonly type = SEND_VERIFICATION_SMS;
  constructor(public payload: any) { }
}

export class SendVerficationVoiceAction implements Action {
  readonly type = SEND_VERIFICATION_VOICE;
  constructor(public payload: any) { }
}


export class ConfirmCodeAction implements Action {
  readonly type = CONFIRM_CODE;
  constructor(public payload: any) { }
}

export type Action =
  LoginAction |
  LoginSuccessAction |
  LoginFailAction |
  RegistrationAction |
  RegistrationSuccessAction |
  RegistrationFailAction |
  SendVerficationSmsAction |
  SendVerficationVoiceAction |
  ConfirmCodeAction;
