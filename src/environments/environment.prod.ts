export const environment = {
  production: true,
  API: 'https://textsecure-service-staging.whispersystems.org'
};
